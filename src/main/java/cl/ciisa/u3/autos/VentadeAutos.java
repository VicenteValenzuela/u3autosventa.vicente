/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.u3.autos;

import cl.ciisa.u3.autos.dao.AutosventaJpaController;
import cl.ciisa.u3.autos.dao.exceptions.NonexistentEntityException;
import cl.ciisa.u3.autos.entity.Autosventa;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author valen
 */
@Path("venta")
public class VentadeAutos {

    private String iddelete;
    

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarAutos(){
        
       
     /*Autosventa av1= new Autosventa();
     av1.setCodigo("4587");
     av1.setColor("verde");
     av1.setMarca("Lada");
     av1.setModelo("picasso");
     av1.setYear("1984");
     
      Autosventa av2= new Autosventa();
     av2.setCodigo("897");
     av2.setColor("rosado");
     av2.setMarca("Volks");
     av2.setModelo("Denmarc");
     av2.setYear("1978");
     
     List<Autosventa> lista=new ArrayList<Autosventa>();
    
     lista.add(av1);
     lista.add(av2);*/
     
     
     AutosventaJpaController dao =new AutosventaJpaController();
    List<Autosventa> lista = dao.findAutosventaEntities();
     
     return Response.ok(200).entity(lista).build();
    
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Autosventa autosventa){
        
        AutosventaJpaController dao =new AutosventaJpaController();
        try {
            dao.create(autosventa);
        } catch (Exception ex) {
            Logger.getLogger(VentadeAutos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(autosventa).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete){
        
        
        try {
            AutosventaJpaController dao =new AutosventaJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(VentadeAutos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("auto eliminado").build();
    }
    
    @PUT
    public Response update(Autosventa autosventa){
        
        try {
            AutosventaJpaController dao =new AutosventaJpaController();
            dao.edit(autosventa);
        } catch (Exception ex) {
            Logger.getLogger(VentadeAutos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(autosventa).build();
        
    }
    
}
    
